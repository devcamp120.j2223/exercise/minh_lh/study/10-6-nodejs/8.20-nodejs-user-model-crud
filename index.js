// câu lệnh này tương tự câu lệnh import express
const express = require("express");

// import mongoose
const mongoose = require('mongoose')
// import schema
// const drinkModel = require('./app/model/drinkModel');
// const voucherModel = require('./app/model/voucherModel');
// const orderModel = require('./app/model/orderModel');
// const userModel = require('./app/model/userModel');

// import router
const drinkRouter = require('./app/router/drinkRouter')
const voucherRouter = require('./app/router/voucherRouter')
const userRouter = require('./app/router/userRouter')
// import thư viện path
const path = require("path")

// khởi tạo app express
const app = express();

// khai báo middleware đọc được json
app.use(express.json());

// khai báo middleware đọc dữ liệu utf-8
app.use(express.urlencoded({
    extended: true
}))

app.use(express.static(__dirname + `/views`))

// khai báo cổng của project
const port = 8000;

mongoose.connect("mongodb://localhost:27017/CRUD_Pizza365", (err) => {
    if (err) {
        throw err;
    }
    console.log("Connect mongoDB successfully")
})

// khai báo API dạng get "/" sẽ chạy vào đây
// call back function là một tham số của hàm khác và nó sẽ được thực thi ngay sau khi hàm kia được gọi
// app.get("/", (request, response) => {
//     console.log(__dirname);
//     response.sendFile(path.join(__dirname + "/views/index.html"))
// })

// chạy router
app.use("/", drinkRouter)
app.use("/", voucherRouter)
app.use("/", userRouter)
// chạy app express
app.listen(port, () => {
    console.log("app listening on port (ứng dụng đang chạy trên cổng)" + port)
})
