// khởi tạo bộ thư viện
const express = require('express');
// import module
const { createUser, getAllUser, getUserById, updateUserById, deleteUserById } = require('../controller/userController')
//khởi tạo router
const router = express.Router();
// create new user
router.post("/user", createUser);
// get all user
router.get("/user", getAllUser)
// get user by id
router.get("/user/:userId", getUserById)
// get user update by id
router.put("/user/:userId", updateUserById)
// delate user
router.delete("/user/:userId", deleteUserById)
// export dữ liệu thành 1 module
module.exports = router;